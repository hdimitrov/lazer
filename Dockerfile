FROM node:15
WORKDIR /usr/src/app
ADD backend /usr/src/app
RUN npm install
EXPOSE 4004
CMD [ "node", "index.js" ]

This is literally me ripping off spatie/ray because I'm too cheap to pay for it.

To add this to your php code you'll need to add in hdimitrov/lazer to your dependency list.
To host the lazer server, you'll need to either run the current docker-compose file or add a similar thing to your existing setup.

To configure it, you'll need to add a line to your env file that looks like:

 LAZER_URL=http://lazer:4004/data


if your environment isn't docker (and it should be) you can use http://localhost:4004/data and the provided docker compose.

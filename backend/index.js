import express from 'express';
import cors from 'cors';
import {Server as io} from 'socket.io';
import http from 'http';
import path from "path";
const app = express()
const __dirname = path.resolve();
const server = http.Server(app);
const socket = new io(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});
app.use(express.json());
app.use(cors());
app.use('/css', express.static('html/css'))
app.use('/js', express.static('html/js'))

const data = []
app.get('/', (req, res) => {
    res.sendFile('html/index.html', {root: __dirname});
})
app.get('/data', (req, res) => {
    res.json({data});
})

app.post('/data', (req, res) => {
    req.body.date = new Date().toString();
    data.push(req.body)
    socket.emit('newEntry', req.body);
    if(data.length > 400) {
        data.pop();
    }
    res.json({success: true});
})

server.listen(4004);

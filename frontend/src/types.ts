/* eslint-disable @typescript-eslint/no-unused-vars */


export interface BaseEntry {
  category: string;
  data: any;
  stacktrace: any;
}

export interface Entry extends BaseEntry {
  date: Date;
}

export interface RawEntry extends BaseEntry {
  date: string;
}
